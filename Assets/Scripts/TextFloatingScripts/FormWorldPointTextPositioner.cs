using UnityEngine;

public class FormWorldPointTextPositioner : IFloatingTextPositioner
{
	private readonly Camera _camera;
	private readonly Vector3 _worldPosition;
	private float _timeToLive;
	private readonly float _speed;
	private float _offset;

	/* camera = where the object should be based on world position(center screen, top left etc)
	 * worldPosition = takes world position somewhere in game level
	 * time to live = amount of seconds we want the text to live
	 * speed = how fast we want the text to go up
	 */
	public FormWorldPointTextPositioner(Camera camera, Vector3 worldPosition, float timeToLive, float speed)
	{
		_camera = camera;
		_worldPosition = worldPosition;
		_timeToLive = timeToLive;
		_speed = speed;
	}

	public bool GetPosition(ref Vector2 position, GUIContent content, Vector2 size)
	{
		if((_timeToLive -= Time.deltaTime) <=0)
			return false;

		var screenPosition = _camera.WorldToScreenPoint (_worldPosition);
		position.x = screenPosition.x - (size.x / 2);
		position.y = Screen.height - screenPosition.y - _offset;


		_offset += Time.deltaTime * _speed;
		return true;
	}
}


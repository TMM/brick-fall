﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour
{
	static bool sawOnce = false;	
	public AudioSource MusicSoundSource;	
	public AudioClip MusicSoundClip;

	public void Awake () 
	{
		MusicSoundSource = GetComponent<AudioSource>();
		sawOnce = false;
	}

	// Use this for initialization
	void Start ()
	{
		if(!sawOnce)
		{
			GetComponent<SpriteRenderer>().enabled = true;
			Time.timeScale = 0;
		}
		sawOnce = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Time.timeScale == 0 && Input.GetKeyDown(KeyCode.Space))
		{
			MusicSoundSource.PlayOneShot(MusicSoundClip, 0.6f);
			MusicSoundSource.Play();
			Time.timeScale = 1;
			GetComponent<SpriteRenderer>().enabled = false;			
		}
	}
}

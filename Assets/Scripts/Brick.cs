﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{
	private float MaxFallSpeed = 28.0f;
	private float fallSpeed = 12;
	private bool Trigger;
	private float despawnTime;
	private bool IsFalling;
	Animator anim;	
	public AudioSource HitSound;	
	public AudioClip HitClip;
	
	void Start ()
	{
		anim = GetComponent<Animator>();
		HitSound = GetComponent<AudioSource>();
		Trigger = false;
		IsFalling = true;
	}

	void PlaySound(bool points)
	{
		HitSound.PlayOneShot(HitClip, 1.0f);
		HitSound.Play();
		anim.SetBool("Trigger", Trigger);
		if (points)
		{
			ScoreUi.AddPoint (1);
			FloatingText.Show (string.Format ("+1"), "PointsText", new FormWorldPointTextPositioner (Camera.main, transform.position, 2.5f, 50/*pixel/sec*/));
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		IsFalling = false;
		Debug.Log ("OnTriggerEnter2D Brick)" + col.name);
		if (col.name == "Player_stand")
		{
			Trigger = true;
			col.GetComponent<Player>().Kill();
			despawnTime = 0.3f;
			PlaySound(false);
		}
		else
		{
			Trigger = true;
			Debug.Log("Floor Hit");
			despawnTime = 0.3f;
			PlaySound(true);
		}
	}
	
	private void DeleteObject(float diff)
	{
		if (!Trigger)
			return;
		
		if (despawnTime <= diff)
		{
			Destroy (this.gameObject);
		}
		else
			despawnTime -= diff;
	}
	
	private float GetSpeed()
	{
		float speed = fallSpeed;
		switch (ScoreUi.score)
		{
		case 20: ++speed; break;
		case 50: ++speed; break;
		case 80: ++speed; break;
		case 110: ++speed; break;
		case 150: ++speed; break;
		case 280: ++speed; break;
		case 300: ++speed; break;
		case 350: ++speed; break;
		case 400: ++speed; break;
		case 450: ++speed; break;
		case 500: ++speed; break;
		case 550: ++speed; break;
		case 650: ++speed; break;
		case 680: ++speed; break;
		case 700: ++speed; break;
		case 850: ++speed; break;
		case 900: ++speed; break;
		case 1050: ++speed; break;
		case 1150: ++speed; break;
		default: break;
		}
		return speed;
	}
	void LateUpdate()
	{
		if (fallSpeed <= MaxFallSpeed)
			fallSpeed = GetSpeed();
		else
			fallSpeed = MaxFallSpeed;
		
		if(IsFalling)
			transform.Translate (Vector3.down * fallSpeed * Time.deltaTime, Space.World);
	}
	
	void Update()
	{
		DeleteObject(Time.deltaTime);
	}
}

﻿using UnityEngine;
using System.Collections;

public class ScoreUi : MonoBehaviour
{
	public static int score = 0;
	static int highScore = 0;
	Player plr;
	
	static ScoreUi instance;
	
	static public void AddPoint(int value)
	{
		if(instance.plr.dead)
			return;
		
		score += value;
		
		if(score > highScore)
			highScore = score;
		
		switch (score)
		{
		case 5:
			Player.NewSpeed(-1);
			Player.CanJump = true;
			FloatingText.Show (string.Format ("Jump Unlocked!"), "JumpUnlock", new FormWorldPointTextPositioner (Camera.main, new Vector3(0,0,0), 6.0f, 20/*pixel/sec*/));
			break;
		case 25:	Player.NewSpeed(-2); break;
		case 45:
		{
			Player.NewSpeed(-1);
			break;
		}
		case 65:	Player.NewSpeed(-2); break;
		case 120:	Player.NewSpeed(-1); break;
		case 150:	Player.NewSpeed(-2); break;
		case 200:	Player.NewSpeed(-1); break;
		default:
			break;
		}
	}
	
	static public int GetScore()
	{
		return score;
	}
		
	void Start()
	{
		instance = this;
		GameObject player_go = GameObject.FindGameObjectWithTag("Player");
		if(player_go == null) {
			Debug.LogError("Could not find an object with tag 'Player'.");
		}
		
		plr = player_go.GetComponent<Player>();
		score = 0;
		highScore = PlayerPrefs.GetInt("highScore", 0);
	}
	
	void OnDestroy()
	{
		instance = null;
		PlayerPrefs.SetInt("highScore", highScore);
	}
	
	void Update ()
	{
		guiText.text = "Score: " + score + "\nHigh Score: " + highScore;
	}
}

﻿using UnityEngine;
using System.Collections;

public class ToadScript : MonoBehaviour {

	private float FallSpeed = 12.0f;
	private float MoveSpeed = 2.0f;
	private bool IsFalling, IsActive, Stay, Left;
	private float _activateTime;
	private int Turns;
	Animator anim;

	public void Awake () 
	{
		IsFalling = true;
		IsActive = false;
		Stay = false;
		Left = true;
		Turns = 0;
	}
	// Use this for initialization
	void Start ()
	{		
		IsFalling = true;
		IsActive = false;
		Stay = false;
		Left = true;
		Turns = 0;
		anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		Debug.Log ("OnTriggerEnter2D Toad:" + col.name);
		if (col.name == "Player_stand")
		{
			Stay = true;
			IsFalling = false;
			anim.SetBool("IsDead", IsFalling);
			Turns = 6;
			col.GetComponent<Player>().Kill();
		}
		else if (col.name == "Floor")
		{
			//transform.position.y += 1.0f;;
			Stay = true;
			IsFalling = false;
			_activateTime = 4;
			anim.SetBool("IsFalling", IsFalling);
			anim.SetBool("IsActive", false);
		}
		else if (col.name == "Margins")
		{
			Turns++;
			if(!Left)
				Left = true;
			else
				Left = false;

			anim.SetBool("IsLeft", Left);
		}
		else return;
	}

	void LateUpdate()
	{		
		if(IsFalling)
			transform.Translate (Vector3.down * FallSpeed * Time.deltaTime, Space.World);

		if (IsActive)
		{
			if(Left)
				transform.Translate (Vector3.right * MoveSpeed * Time.deltaTime, Space.World);
			else
				transform.Translate (Vector3.left * MoveSpeed * Time.deltaTime, Space.World);
		}
	}

	void ToatActivate(float diff)
	{
		if(_activateTime <= diff)
		{			
			IsActive = true;
			Stay = false;
			anim.SetBool("IsActive", IsActive);
			anim.SetBool("IsLeft", Left);
		}
		else _activateTime -= diff;
	}
	// Update is called once per frame
	void Update ()
	{
		if(Turns >= 6)
			Destroy (this.gameObject);

		// Debug.Log (_activateTime);
		if(Stay)
			ToatActivate(Time.deltaTime);
	}
}

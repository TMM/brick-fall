﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{

	private static float speed = 15f;
	private float jumpSpeed = 15.0f;
	private Vector3 target;

	public GameObject brick;
	public GameObject RedMonster;
	public GameObject Toad;

	Animator anim;
	public AudioSource JumpSound;	
	public AudioClip JumpClip;
	public bool godMode = false;
	public bool dead = false;
	public static bool CanJump = false;
	private float deathCooldown;
	private float SpawnBrick, SpawnRedMonster, SapwnToad = 0.0f;

	public static void NewSpeed(float minusSpeed)
	{
		float playerSpeed = speed;
		playerSpeed = playerSpeed + minusSpeed;
		speed = playerSpeed;
	}

	public void Kill()
	{	
		//Debug.Log ("Kill void hit");
		if(godMode)
			return;
		
		transform.Rotate (Vector3.forward * Random.Range(-90,90));
		//anim.SetTrigger("Death");
		
		FloatingText.Show (string.Format ("Game Over!"), "GameOver", new FormWorldPointTextPositioner (Camera.main, new Vector3(0,0,0), 6.0f, 20/*pixel/sec*/));
		dead = true;
		deathCooldown = 1.5f;
	}


	// Use this for initialization
	void Start ()
	{
		CanJump = false;
		JumpSound = GetComponent<AudioSource>();
		anim = GetComponent<Animator>();
		target = transform.position;
		if(target == null)
		{
			Debug.Log("Transform missing");
			return;
		}
		SpawnBrick = 5.0f;
		SpawnRedMonster = Random.Range(15, 60);
		SapwnToad = Random.Range(22, 30);
	}

	private void Spawn(int id)
	{
		//Debug.Log ("Spawn hit");
		switch (id)
		{
		case 0:
			Instantiate (brick, new Vector3(transform.position.x, transform.position.y + 10.0f, 0), Quaternion.identity);
			break;
		case 1:
			Instantiate (RedMonster, new Vector3(transform.position.x, transform.position.y + 10.0f, 0), Quaternion.identity);
			break;
		case 2:
			Instantiate (Toad, new Vector3(transform.position.x, transform.position.y + 10.0f, 0), Quaternion.identity);
			break;
		default:
			break;
		}
	}

	private void SpawnCreatures(float diff)
	{
		//Debug.Log (SpawnBrick);
		if (SpawnBrick <= diff)
		{
			SpawnBrick = Random.Range(1,6);
			Spawn(0);
		}
		else
			SpawnBrick -= diff;

		if (SpawnRedMonster <= diff)
		{
			SpawnRedMonster = Random.Range(15, 60);
			Spawn(1);
		}
		else
			SpawnRedMonster -= diff;

		if (SapwnToad <= diff)
		{
			SapwnToad = Random.Range(25, 80);
			Spawn(2);
		}
		else
			SapwnToad -= diff;
	}

	void FixedUpdate()
	{
		if(dead)
			return;

		float move = Input.GetAxis ("Horizontal");
		
		anim.SetFloat("Speed", Mathf.Abs(move));

		if (Input.GetKeyDown(KeyCode.Space) && transform.position.y < -4.36f && CanJump)
		{
			if(rigidbody2D.velocity.y < 3.2f)
			{
				JumpSound.PlayOneShot(JumpClip, 0.6f);
				JumpSound.Play();
				rigidbody2D.velocity = new Vector2 (move * speed, rigidbody2D.velocity.y + jumpSpeed);
			}
		}

		rigidbody2D.velocity = new Vector2 (move * speed, rigidbody2D.velocity.y);
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		if(dead)
		{
			deathCooldown -= Time.deltaTime;
			
			if(deathCooldown <= 0)
			{
				Application.LoadLevel( Application.loadedLevel );
			}
		}
		else
			SpawnCreatures (Time.deltaTime);
		//float h = Input.GetAxis ("Horizontal") * speed;
		//rigidbody2D.AddForce (new Vector2 (h,0));

	}
}

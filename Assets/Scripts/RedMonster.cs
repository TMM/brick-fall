﻿using UnityEngine;
using System.Collections;

public class RedMonster : MonoBehaviour
{
	private float FallSpeed = 15.0f;
	private float RaiseSpeed = 3.0f;
	private Vector3 _RedMonster;
	private bool IsFalling, IsReturning, Stay;
	private float DespawnTime, StayTime;	
	public AudioSource HitSound;	
	public AudioClip HitClip;
	
	public void Awake () 
	{
		IsFalling = true;
		IsReturning = false;
		Stay = false;
	}
	
	void Start ()
	{
		_RedMonster = transform.position;
		IsFalling = true;
		IsReturning = false;
		Stay = false;
		DespawnTime = -2;
		HitSound = GetComponent<AudioSource>();
	}
	
	private void PlaySound()
	{
		HitSound.PlayOneShot(HitClip, 1.0f);
		HitSound.Play();
		ScoreUi.AddPoint(10);
		FloatingText.Show (string.Format ("+10"), "PointsText", new FormWorldPointTextPositioner (Camera.main, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), 2.5f, 50/*pixel/sec*/));

	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		Debug.Log ("OnTriggerEnter2D Monster:" + col.name);
		if (col.name == "Player_stand")
		{
			col.GetComponent<Player>().Kill();
			StayTime = 3;
			DespawnTime = 5;
			Stay = true;
			IsFalling = false;
		}
		else
		{
			StayTime = 3;
			DespawnTime = 5;
			Stay = true;
			IsFalling = false;
			PlaySound ();
		}
	}
	
	private void DeleteRedMonster(float diff)
	{
		if (!Stay)
			return;
		
		if (StayTime <= diff)
		{		
			IsReturning = true;
		}
		else
			StayTime -= diff;
		
		if (!IsReturning)
			return;
		
		if (DespawnTime <= diff)
		{
			Destroy(this.gameObject);
		}
		else
			DespawnTime -= diff;
	}
	
	void LateUpdate()
	{
		if(IsReturning)
			transform.Translate (Vector3.up * RaiseSpeed * Time.deltaTime, Space.World);
		
		if(IsFalling)
			transform.Translate (Vector3.down * FallSpeed * Time.deltaTime, Space.World);
		
	}
	
	void Update()
	{
		DeleteRedMonster(Time.deltaTime);
	}
}
